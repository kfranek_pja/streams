package services;

import model.ClientDetails;
import model.Order;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class OrdersService {

    public static List<Order> findOrdersWhichHaveMoreThan5OrderItems(List<Order> orders){
	    return orders.stream()
		.filter(order -> (order.getItems().size() > 50))
		.collect(Collectors.toList());
    }

    public static ClientDetails findOldestClientAmongThoseWhoMadeOrders(List<Order> orders) {
    	return orders.stream()
    	.map(o -> o.getClientDetails())
    	.sorted((d1,d2) -> (d2.getAge() - d2.getAge()))
    	.findFirst()
		.get();
    }

    public static Order findOrderWithLongestComments(List<Order> orders) {
    	return orders.stream()
    	.sorted((o1,o2) -> (o2.getComments().length() - o1.getComments().length()))
    	.findFirst()
    	.get();
    }

    public static String getNamesAndSurnamesCommaSeparatedOfAllClientsAbove18YearsOld(List<Order> orders) {
    	return orders
    	.stream()
    	.filter(o -> (o.getClientDetails().getAge() >= 18))
    	.map(o -> (o.getClientDetails().getName() + ' ' + o.getClientDetails().getSurname()))
    	.collect(Collectors.joining(", "));
    }

    public static List<String> getSortedOrderItemsNamesOfOrdersWithCommentsStartingWithA(List<Order> orders) {
    	return orders
    	.stream()
    	.filter(o -> (o.getComments().substring(0,1) == "A" || o.getComments().substring(0,1) == "a"))
    	.flatMap(o -> o.getItems().stream())
    	.map(i -> i.getName())
    	.sorted(String::compareTo)
    	.collect(Collectors.toList());

    }
    
    public static void printCapitalizedClientsLoginsWhoHasNameStartingWithS(List<Order> orders) {
    	List<String> filteredLogins = orders
    	.stream()
    	.map(o -> o.getClientDetails().getLogin())
    	.filter(l -> (l.substring(0,1) == "S"))
    	.collect(Collectors.toList());
    	System.out.println(filteredLogins);
    }

    public static Map<ClientDetails, List<Order>> groupOrdersByClient(List<Order> orders) {
    	return orders
    	.stream()
    	.collect(Collectors.groupingBy(Order::getClientDetails));
    }

    public static Map<Boolean, List<ClientDetails>> partitionClientsByUnderAndOver18(List<Order> orders) {
        return orders
        .stream()
        .map(o -> o.getClientDetails())
        .collect(Collectors.partitioningBy(c -> c.getAge() < 18));
    }

}
