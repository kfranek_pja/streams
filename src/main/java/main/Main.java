package main;

import java.util.ArrayList;
import java.util.List;

import model.ClientDetails;
import model.Order;
import model.OrderItem;
import services.OrdersService;

public class Main {

	public static void main(String[] args) {
		Order o1, o2, o3;
		List<OrderItem> oi1, oi2, oi3;
		OrderItem oi;
		
		
		
		/*oi1 = new ArrayList<OrderItem>();
		for(int i = 0; i < 123; i++){
			oi = new OrderItem();
			oi1.add(oi);
		}
		o1 = new Order();
		o1.setItems(oi1);
		
		oi2 = new ArrayList<OrderItem>();
		for(int i = 0; i < 51; i++){
			oi = new OrderItem();
			oi2.add(oi);
		}
		o2 = new Order();
		o2.setItems(oi2);
		
		oi3 = new ArrayList<OrderItem>();
		for(int i = 0; i < 51; i++){
			oi = new OrderItem();
			oi3.add(oi);
		}
		o3 = new Order();
		o3.setItems(oi3);*/
		
		/*ClientDetails cd1, cd2, cd3;
		o1 = new Order();
		cd1 = new ClientDetails();
		cd1.setAge(12);
		o1.setClientDetails(cd1);
		
		o2 = new Order();
		cd2 = new ClientDetails();
		cd2.setAge(24);
		o2.setClientDetails(cd2);
		
		o3 = new Order();
		cd3 = new ClientDetails();
		cd3.setAge(16);
		o3.setClientDetails(cd3);
		
		List<Order> os = new ArrayList<Order>();
		os.add(o1);
		os.add(o2);
		os.add(o3);*/
		
		/*ClientDetails cd1, cd2, cd3;
		o1 = new Order();
		o1.setComments("012345678901cvv2345");
		
		o2 = new Order();
		o2.setComments("0123456789012kskdskmskfmds");
		
		o3 = new Order();
		o3.setComments("0123xkdfdpfldskfdlklskfldkflkdslfkdsl456");
		
		List<Order> os = new ArrayList<Order>();
		os.add(o1);
		os.add(o2);
		os.add(o3);*/
		
		ClientDetails cd1, cd2, cd3;
		o1 = new Order();
		cd1 = new ClientDetails();
		cd1.setName("Andrzej");
		cd1.setSurname("Nowak");
		cd1.setAge(16);
		o1.setClientDetails(cd1);
		
		o2 = new Order();
		cd2 = new ClientDetails();
		cd2.setName("Gunter");
		cd2.setSurname("Stefanowicz");
		cd2.setAge(21);
		o2.setClientDetails(cd2);
		
		o3 = new Order();
		cd3 = new ClientDetails();
		cd3.setName("Jan");
		cd3.setSurname("Jarzabek");
		cd3.setAge(18);
		o3.setClientDetails(cd3);
		
		List<Order> os = new ArrayList<Order>();
		os.add(o1);
		os.add(o2);
		os.add(o3);
		
		System.out.println(OrdersService.getNamesAndSurnamesCommaSeparatedOfAllClientsAbove18YearsOld(os));
		
	}
}
